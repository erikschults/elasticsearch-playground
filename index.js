import Koa from 'koa';

const app = new Koa();

app.use(ctx => {
  ctx.body = 'Hello world';
});

app.listen(process.env.PORT, () => {
  console.log(`Listening or port ${process.env.PORT}`);
});

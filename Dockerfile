FROM node:12-alpine
USER root

RUN apk add --no-cache bash
RUN apk add --no-cache bash-doc
RUN apk add --no-cache bash-completion

ENV NODE_ENV=development \
	  PORT=3745

WORKDIR /app
